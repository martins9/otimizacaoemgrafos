class Dijkstra(Grafo , v1 , v2):
	
	#vertices não visitados, menor caminho encontrado
	VVnaoVisitados,menorCaminho = [] 
	# Vertice visitado, Vertice vizinho do visitado, vertices do menor caminho
	VVisitado,VVizinho,VMenorCaminho,i = int
	ResultMenorCaminho = 0

	menorCaminho.append(v1)

	#Distancia 9999 inicial
	for i in range(0, Grafo.Vertices.length()):
	
		if (Grafo.vertex(i).name == v1.name):
			Grafo.vertex(i).distancia = 0
		else:
            #Inicia os vertices não visitados
			Grafo.vertex(i).distancia = 9999
            this.VVnaoVisitados.append(Grafo.vertex(i))
	        this.VnaoVisitados.sort()

		#Para todos os vertices
        while not is_empty(this.VnaoVisitados) :

        #Toma-se sempre o vertice com menor distancia, que eh o primeiro da lista
        VVisitado = this.VnaoVisitados(0)
        print("Usando o vertice:  " + VVisitado);
        
        #Para cada vizinho (cada aresta), calcula-se a sua possivel distancia, somando a distancia do vertice VVisitado com a da aresta
        #correspondente. Se essa distancia for menor que a distancia do vizinho, esta eh VVisitadoizada.

        for i in range(0,VVisitado.neighbors.length()):

            vizinho = VVisitado.neighbors(i)
            print("Recebendo o vizinho de " + VVisitado.name + ": "+ vizinho.name)
            
            if (vizinho.visitado == false):
                #Comparando a distancia do vizinho com a possivel distancia
                if (vizinho.distancia > (VVisitado.distancia + VVisitado.neighbor(i).Peso)):
                    vizinho.distancia = VVisitado.distancia + VVisitado.neighbor(i).Peso
                    vizinho.pai = VVisitado;        
                    #Se o vizinho eh o vertice procurado, e foi feita uma mudança na distancia, a lista com o menor caminho
                    #anterior eh apagada, pois existe um caminho menor vertices pais, ateh o vertice origem.
                    if (vizinho == v2):
                        menorCaminho.clear()
                        VMenorCaminho = vizinho
                        
                        #Soma os pesos das arestas que pertencem ao menor caminho possivel
                        ResultMenorCaminho = ResultMenorCaminho + vizinho.distancia
                        menorCaminho.append(vizinho);

                        while (VMenorCaminho.pai != null):
                            menorCaminho.append(VMenorCaminho.pai)
                            VMenorCaminho = VMenorCaminho.pai
                            menorCaminho.sort()
            #Atributos de visitado
            VVisitado.visitado = true
            this.VnaoVisitados.remove(VVisitado)

            VnaoVisitados.sort()
            print("Nao foram visitados ainda:"+VnaoVisitados)
        
        print("O valor do menor caminho feito pelo algoritmo é:"+ ResultMenorCaminho)
        return menorCaminho;